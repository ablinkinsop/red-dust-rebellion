# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'squib'

data = Squib.csv file: 'manifest.csv'

# We set dpi and columns to weird numbers for Tabletop Simulator asset creation:
# https://kb.tabletopsimulator.com/custom-content/asset-creation/#image-resolution
#
# TODO: Also produce a more normal output, perhaps?
Squib::Deck.new(cards: data['Title'].size, layout: 'layout.yml', width: '2.5in', height: '3.5in', dpi: 512 / 2.5) do
  background color: 'white'
  rect color: 'black'

  ops = []
  data['Ops'].each_with_index do |val, i|
    ops << i if val
  end
  text str: data['Ops'], layout: :ops, range: ops

  # Flashpoint highlighting.
  fp = []
  data['FP?'].each_with_index do |val, i|
    fp << i if val == 'Yes'
  end
  rect stroke_color: '#0000',
       fill_color: '(256,256,10)(256,256,256) red@0.0 white@1.0',
       layout: :eligibility, range: fp

  # Eligibility order.
  eligibility = []
  data['f1'].each_with_index do |f1, i|
    eligibility << "#{f1} #{data['f2'][i]} #{data['f3'][i]}".strip
  end
  text(str: eligibility, layout: :eligibility) do |embed|
    embed.png key: 'MG', file: 'public/mg.png', width: 96, height: 96
    embed.png key: 'CORP', file: 'public/corp.png', width: 96, height: 96
    embed.png key: 'RD', file: 'public/rd.png', width: 96, height: 96
    embed.png key: 'CR', file: 'public/cr.png', width: 96, height: 96
  end

  text str: data['Title'], layout: :title

  unshaded_text = []
  data['Top Title'].each_with_index do |title, i|
    unshaded_text << "<i>#{title}</i>\n#{data['Top Text'][i]}"
  end
  text str: unshaded_text, layout: :unshaded_text

  has_shaded = []
  shaded_text = []
  data['Bottom Title'].each_with_index do |title, i|
    if (!title.nil? && title.upcase != 'N/A') ||
       (!data['Bottom Text'][i].nil? && data['Bottom Text'][i].upcase != 'N/A')
      has_shaded << i
      shaded_text << "<i>#{title}</i>\n#{data['Bottom Text'][i]}"
    else
      shaded_text << ''
    end
  end
  rect color: '#ccc', layout: :bot_panel, range: has_shaded
  text str: shaded_text, layout: :shaded_text, range: has_shaded

  text str: data['#'], layout: :id
  sheet = {}
  data['#'].each_with_index do |id, i|
    key = if id.is_a?(String) && id =~ /^RD\d+/
            'rd'
          elsif id.is_a?(String) && id =~ /^CR\d+/
            'cr'
          elsif (id == 'P') || ((1..48).cover? id.to_i)
            'event'
          else
            'exp'
          end
    (sheet[key] ||= []) << i
  end

  # Sheet for each of the three card backs.
  save_sheet prefix: 'tts_event_', columns: 8, range: sheet['event']
  save_sheet prefix: 'tts_cr_', columns: 8, range: sheet['cr']
  save_sheet prefix: 'tts_rd_', columns: 8, range: sheet['rd']
end
