# Red Dust Rebellion

A COIN on Mars.

Steam Workshop [Id](https://steamcommunity.com/sharedfiles/filedetails/?id=2197390661): 2197390661

## Development

RDR uses [Squib](https://squib.rocks) to generate card images. The easiest way to get Squib working is through Bundler:

```
bundle install
```
